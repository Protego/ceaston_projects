# README #

This is the repository for Christopher Easton, a student at Western Oregon University. The main project in this repository, IPGMMS, is meant as a capstone project for a Bachelor's degree in Computer Science.

### The contents of this repository includes: ###

1. IPGMMS
    * The primary group project for this repository.
2. FakeNewsProject 
    * An in class project that was used for the initial learning of the Scrum process.
3. InventoryManager
	* An IOS Applicate for managing inventory. Initial exposure to iOS development.
    
## StarTech Members ##

* Sarah Alvarado
* Christopher Easton
* Wayne Rose
* Cameron Stanavige

## StarTech Project ##

### IPGMMS ###

The project StarTech has taken on for their senior capstone is the creation of the International Professional Groomers Members Management Service. This project is to assist [International Professional Groomers, Inc](http://www.ipgicmg.com/) (IPG) with the managing and promotion of their members.

#### Vision Statement ####

> For international certification companies who need to manage memberships, certifications, and client accessibility. The International Professional Groomers Member Management Service (IPGMMS) is a publically available web application containing all that is necessary for the managing, certifying, informing, and locating of professional groomers all over the world; providing a valuable resource for IPG admin, pet groomers, and clients alike. This application will assist IPG admin in the certification of new members, as well as the creating and updating accounts and the management of already certified members. Secondly, this application will provide a simple and organized way for potential members to begin their certification process and for already certified members to update and upgrade their certifications. Lastly, this application will provide a variety of convenient ways for clients to search for and find all needed information about IPG members in their area or area of choice. Unlike the current lack of pet groomer websites, our product will provide an easy-to-use centralized hub for the entire spectrum of user needs regarding professional groomers.

### How do I get started? ###

The [starting](starting.md) file has information about how to clone and set up this project on your local machine  and how to become a contributor.

### Contributing ###

For contributing and coding guildines when working on this project, see these [Guidelines](guidelines.md).
Here is a list of [contributors](contributors.md) to this project. Please add your name to this document when you become a contributor.

### Software Construction Process ###

For this project, we are following Agile methodologies, specifically Disciplined Agile Delivery process framework. 
We follow a two-week sprint cycle that involves daily SCRUM meetings, weekly sprint planning sessions, an end of sprint review and retrospective. 
To contribute to this project, one will need to be involved in this process with the StarTech team.

### Tools, Configurations, and Packages ###

[Tools](tools.md) is a list of all the software, libraries, tools, packages, and versions used on this project. Make sure you are using the same ones to avoid any compatability errors.

## Personal Projects ##

### InventoryManager ###

#### Vision Statement ####

> 
